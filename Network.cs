﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace KeyTrain
{
    static class Network
    {
        public static string Token { get; private set; } = "";

        public static async Task Upload(object body)
        {
            try
            {

                var json = JsonSerializer.Serialize(body);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                using var client = new HttpClient();

                client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", Token);
                var response = await client.PostAsync("http://192.168.0.200:8000/enter", data);
                StatusBar.SyncDone();
                if (!response.IsSuccessStatusCode)
                {
                    StatusBar.Error($"{(int)response.StatusCode} : { await response.Content.ReadAsStringAsync()}");
                }
                Trace.WriteLine(response);
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
                StatusBar.Error(e.Message);
            }
        }
        //TODO: Return token or null instead
        public static async Task<bool> TryLogin()
        {
            try
            {
                StatusBar.Syncing(message: "Attempting login...");
                using var client = new HttpClient();
                var json = JsonSerializer.Serialize(new
                {
                    username = ConfigManager.Settings["username"],
                    password = ConfigManager.Settings["password"]
                });
                var data = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync("http://192.168.0.200:8000/login", data);
                if (!response.IsSuccessStatusCode)
                {
                    StatusBar.Error($"{(int)response.StatusCode} : { await response.Content.ReadAsStringAsync()}");
                    return false;
                }
                var str = await response.Content.ReadAsStringAsync();
                var response_content = JsonSerializer.Deserialize<LoginResponse>(str);
                if (response_content.status == "ok")
                {
                    StatusBar.SetStatus("Connection successful!", StatusBar.Icons.sync_done);
                    Token = response_content.token;
                    return true;
                }
                else
                {
                    StatusBar.Error(response_content.msg);
                }
                    
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
                StatusBar.Error("Error: Could not connect.");
            }
            return false;
        }

        class LoginResponse
        {
            public string token { get; set; }
            public string status{ get; set; }
            public string msg   { get; set; }
        }
    }
}
