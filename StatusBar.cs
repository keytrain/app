﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeyTrain
{
    static class StatusBar
    {
        public static MainWindow window;
        public struct Icons
        {
            public const string sync = "icons/appbar.refresh.png";
            public const string sync_done  = "icons/appbar.check.png";
            public const string error = "icons/appbar.alert.png";

        }
        //static int minDelay = 2000;

        public static void  SetMessage(string message) => window.status_message.Text = message;

        public static void SetIcon(string path = "")
        {
            if(path == "")
            {
                return; //I would hide the image here, but appearently that's not that easy.
            }
            //Uri uri;
            //if(Uri.TryCreate($"pack://application:,,,/KeyTrain;component/{path}", 
            //    UriKind.RelativeOrAbsolute, out uri) == false)
            //{
            //    Error($"Error: Could not load icon '{path}'. ", icon:"");
            //    //return;
            //}

            window.status_icon.Source = new System.Windows.Media.Imaging.BitmapImage(
                new Uri($"pack://application:,,,/KeyTrain;component/{path}"));
        }

        public static void SetStatus(string message, string icon)
        {
            SetMessage(message);
            SetIcon(icon);
        }

        public static void Syncing(string message = "Syncing...") => SetStatus(message, Icons.sync);
        public static void SyncDone(string message = "Synced") => SetStatus(message, Icons.sync_done);
        public static void Error(string message = "Error", string icon = Icons.error) => SetStatus(message, icon);
    }
}
